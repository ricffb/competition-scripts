# Add this file (or symlink it) as ~/.competition-configure-verifiercloud.sh if you want to execute run collections on a VerifierCloud.

BENCHMARKSCRIPT="$(dirname "$0")/../../benchexec/contrib/vcloud-benchmark.py"
VERIFIERCLOUDOPTIONS="--vcloudAdditionalFiles . --vcloudClientHeap 4000 --cgroupAccess"

export OPTIONSVERIFY="$OPTIONSVERIFY --vcloudPriority HIGH"
export OPTIONSVALIDATE="$OPTIONSVALIDATE --vcloudPriority URGENT --no-ivy-cache"

