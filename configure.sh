# Configuration of variables to initialize the competition environment

set -eao pipefail

SCRIPT_DIR=$(realpath "scripts");
export BENCHMARKSDIR="benchmark-defs";

YEAR=$(yq --raw-output '.year' ${BENCHMARKSDIR}/category-structure.yml)
COMPETITIONNAME=$(yq --raw-output '.competition' ${BENCHMARKSDIR}/category-structure.yml)
COMPETITION=${COMPETITIONNAME}${YEAR#??}  # use two last digits of year, only
TARGETSERVER=`echo ${COMPETITIONNAME} | tr A-Z a-z`
export FILE_STORE_URL_PREFIX="https://${TARGETSERVER}.sosy-lab.org/${YEAR}/results/"

export PATHPREFIX=$(realpath .)
TARGETDIR=${COMPETITIONNAME}
export RESULTSVERIFICATION="results-verified";
export RESULTSVALIDATION="results-validated";
export BINDIR="bin";
export PYTHONPATH="$PATHPREFIX/benchexec:$SCRIPT_DIR";
export BENCHEXEC_PATH="${PATHPREFIX}/benchexec";
BENCHMARKSCRIPT="$(dirname "$0")/../../benchexec/bin/benchexec"
BENCHEXECOPTIONS="--maxLogfileSize 2MB --read-only-dir / --read-only-dir $PATHPREFIX --overlay-dir ./ --hidden-dir /home/"
if [ -e /data ]; then
  BENCHEXECOPTIONS="$BENCHEXECOPTIONS --hidden-dir /data/"
fi
if [ -e /localhome ]; then
  BENCHEXECOPTIONS="$BENCHEXECOPTIONS --hidden-dir /localhome/"
fi

ADDRESS_BOOK=~/.competition-address-book.txt
USER_CONFIG=~/.competition-configure.sh
if [ -e "$USER_CONFIG" ]; then
  source "$USER_CONFIG"
fi
VERIFIERCLOUD_CONFIG=~/.competition-configure-verifiercloud.sh
if [ -e "$VERIFIERCLOUD_CONFIG" ]; then
  source "$VERIFIERCLOUD_CONFIG"
else
  BENCHEXECOPTIONS="$BENCHEXECOPTIONS --set-cgroup-value pids.max=5000"
fi

ACTIONS=${ACTIONS:-"PRODUCE_RESULTS VALIDATE_RESULTS PREPARE_RESULTS"}

export HASHES_BASENAME="fileHashes.json";
export HASHDIR_BASENAME="fileByHash";

export PROPERTIES=$(yq -r '.properties []' benchmark-defs/category-structure.yml)
VALIDATORLIST=$(yq -r '.validators | keys []' benchmark-defs/category-structure.yml);
#VALIDATORLIST="witnesslint-validate-witnesses";
#ACTIONS="VALIDATE_RESULTS"

RESULTSLEVEL="Final";
if [[ -n "$LIMIT_CORES" || -n "$LIMIT_MEMORY" || -n "$LIMIT_TIME" ]]; then
  LIMITSTEXT="\nLimits: The current pre-run results are limited with: $LIMIT_TIME $LIMIT_CORES $LIMIT_MEMORY.\n"
  RESULTSLEVEL="Pre-run";
fi

if [[ "${COMPETITIONNAME}" == "SV-COMP" ]]; then
  VALIDATIONKIND="witnesses";

  WITNESSTARGET="witness.graphml";
  WITNESSGLOBSUFFIX=".graphml";

elif [[ "${COMPETITIONNAME}" == "Test-Comp" ]]; then
  VALIDATIONKIND="test-suites";

  WITNESSTARGET="test-suite.zip";
  WITNESSGLOBSUFFIX=".zip";

  TESTCOMPOPTION="--zipResultFiles";

  LIMIT_VAL_TIME="--timelimit 300";
  TARGETDIR=`echo ${COMPETITIONNAME} | tr A-Z a-z`

else
  echo "Unhandled competition $COMPETITIONNAME" ; false
fi



