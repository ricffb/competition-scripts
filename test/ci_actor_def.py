#!/bin/python3


from dataclasses import dataclass
import io
import json
import os
from pathlib import Path
import re
import subprocess
from typing import IO, Any, Dict, Final, Iterable, List, Optional, Union, cast, overload
import xml.etree.ElementTree as et
from zipfile import ZipFile, ZipInfo
import yaml
import sys

import logging
import requests

BENCHDEF_TEMPLATE = (
    "https://gitlab.com/sosy-lab/sv-comp/bench-defs/-"
    "/raw/main/benchmark-defs/{tool}.xml"
)

BRANCH_TEMPLATE = os.getenv("BRANCH_TEMPLATE",
    "https://gitlab.com/sosy-lab/sv-comp/archives-2023/-/raw/{branch}/2023/{tool}.zip"
)
URL = "https://coveriteam-service.sosy-lab.org/execute"

SCRIPT = Path(__file__).parent

VERIFIER_CVT: Final[Path] = SCRIPT / Path("verifier.cvt")
SPECIFICATION_JAVA: Final[Path] = SCRIPT / Path()
SPECIFICATION_C: Final[Path] = SCRIPT / Path("specification.prp")
PROGRAM_C: Final[Path] = SCRIPT / Path("program.c")
PROGRAM_JAVA: Final[Path] = SCRIPT / Path()


logging.basicConfig(format="[%(levelname)s] %(message)s", level=logging.INFO)
StrPath = Union[str, Path]
FileRead = Union[io.BytesIO, StrPath]


@dataclass
class ActorDef:
    name: str
    tool_info: str
    options: List[str]
    is_java: bool


def get_diff() -> Iterable[str]:
    ret = subprocess.run(
        [
            "git",
            "diff",
            "--name-only",
            "--diff-filter=d",
            "origin/main...",
            "--",
            "./2023/*.zip",
        ],
        stdout=subprocess.PIPE,
    )

    output = ret.stdout.decode(errors="ignore")
    logging.info("Git diff: %s", output)

    return set((Path(path).stem for path in output.splitlines()))


def download_benchdef_xml(tool: str) -> io.BytesIO:
    link = BENCHDEF_TEMPLATE.format(tool=tool)
    resp = requests.get(link)
    return io.BytesIO(resp.content)


def get_link(branch: str, tool: str) -> str:
    return BRANCH_TEMPLATE.format(branch=branch, tool=tool)


def parse_benchdef_options(benchdef: FileRead, tool_name: str) -> ActorDef:

    root = et.parse(benchdef).getroot()

    is_java = any(
        (
            d.get("name").endswith("_java")  # type: ignore
            for d in root.findall("rundefinition")
            if d is not None
        )
    )

    options: List[str] = []
    for option in root.findall("option"):
        name = option.get("name")
        if name is None:
            raise RuntimeError(f"'name' tag missing for an option in xml {benchdef}")
        options.append(name)
        if option.text:
            options.append(option.text)

    tool_info = root.get("tool")
    if tool_info is None:
        raise RuntimeError(f"'tool' tag missing for benchmark xml {benchdef}")

    return ActorDef(tool_name, tool_info, options, is_java)


@overload
def assemble_actor_yml(branch: str, definition: ActorDef, to_file: None) -> None:
    ...


@overload
def assemble_actor_yml(
    branch: str, definition: ActorDef, to_file: IO[bytes] = ...
) -> "yaml._Yaml":
    ...


def assemble_actor_yml(
    branch: str, definition: ActorDef, to_file: Optional[IO[bytes]] = None
):
    # Remote location of the tool
    loc = get_link(branch=branch, tool=definition.name)

    raw = {
        "resourcelimits": {
            "memlimit": "8 GB",
            "timelimit": "2 min",
            "cpuCores": "2",
        },
        "actor_name": definition.name,
        "toolinfo_module": definition.tool_info,
        "archives": [
            {"version": "svcomp23", "location": loc, "options": definition.options}
        ],
        "format_version": "1.2",
    }

    if to_file is not None:
        yaml.dump(raw, to_file)
        return

    return yaml.dump(raw)


def prepare_file_dict(actor_yml: str, is_java: bool):
    program = "program.java" if is_java else "program.c"
    return {
        "verifier.cvt": VERIFIER_CVT.open("rb"),
        "specification.prp": SPECIFICATION_JAVA.open("rb")
        if is_java
        else SPECIFICATION_C.open("rb"),
        "actor.yml": ("actor.yml", actor_yml),
        program: PROGRAM_JAVA.open("rb") if is_java else PROGRAM_C.open("rb"),
    }


def prepare_args(is_java: bool):

    args = {
        "coveriteam_inputs": {
            "verifier_path": "actor.yml",
            "program_path": "program.java" if is_java else "program.c",
            "specification_path": "specification.prp",
            "verifier_version": "svcomp23",
            "data_model": "ILP32",
        },
        "cvt_program": "verifier.cvt",
        "working_directory": "coveriteam",
    }

    if not is_java:
        cvt_inputs = cast(Dict[str, str], args["coveriteam_inputs"])
        cvt_inputs["data_model"] = "ILP32"

    return args


def make_request(args: Dict[str, Any], files):
    jargs = json.dumps(args)

    return requests.post(url=URL, data={"args": jargs}, files=files)


def determine_result(run):
    """
    It assumes that any verifier or validator implemented in CoVeriTeam
    will print out the produced aftifacts.
    If more than one dict is printed, the first matching one.
    """
    verdict = None
    verdict_regex = re.compile(r"'verdict': '([a-zA-Z\(\)\ \-]*)'")
    for line in reversed(run):
        line = line.strip()
        verdict_match = verdict_regex.search(line)
        if verdict_match and verdict is None:
            # CoVeriTeam outputs benchexec result categories as verdicts.
            verdict = verdict_match.group(1)
        if "Traceback (most recent call last)" in line:
            verdict = "EXCEPTION"
    if verdict is None:
        return "UNKNOWN"
    return verdict


def handle_response(tool: str, response: requests.Response):
    output_path = Path(f"output-{tool}.zip")
    with output_path.open("w+b") as fd:
        fd.write(response.content)
        fd.flush()
        fd.seek(0)
        with ZipFile(fd, "r") as zipf, zipf.open("LOG") as log:
            cvt_log = log.read().decode(errors="ignore")
            logging.info(
                "------------------------------------------------------------------\n"
                "The following log was produced by the execution of the CoVeriTeam "
                "program on the server: %s\n"
                "--------------------------------------------------------------"
                "-----------\nEND OF THE LOG FROM REMOTE EXECUTION",
                cvt_log,
            )
            return determine_result(cvt_log.splitlines()), output_path


def prepare_curl_command(args: Dict[str, Any], files: Iterable[str]):

    base = "#!/bin/sh\n\n"
    base += "curl -X POST -H 'ContentType: multipart/form-data' -k \\\n"
    base += "https://coveriteam-service.sosy-lab.org/execute \\\n"
    base += "\t--form args='{}'\\\n".format(json.dumps(args))
    base += "\t--output cvt_remote_output.zip"
    for file in files:
        base += f"\t--form '{file}'=@{file}\\\n"

    return base


def add_call_data(archive: StrPath, args: Dict[str, Any], files: Dict[str, Any]):
    with ZipFile(archive, "a") as zipf:
        for key, fd in files.items():
            print(key)
            try:
                fd.seek(0)
                zipf.writestr(f"data/{key}", fd.read())
            except AttributeError:
                zipf.writestr(f"data/{key}", fd[1])

        curl = prepare_curl_command(args, files)

        info = ZipInfo("data/send_request.sh")
        # make executable
        info.external_attr |= 0o755 << 16
        zipf.writestr(info, curl)


def check_tool(tool: str, branch: str):

    benchdef = download_benchdef_xml(tool=tool)

    actor_def = parse_benchdef_options(benchdef, tool)
    logging.info("Extracted actor def: %s", actor_def)

    yml = assemble_actor_yml(branch, actor_def)
    logging.info("Created yaml:\n%s", yml)

    files = prepare_file_dict(yml, actor_def.is_java)
    args = prepare_args(actor_def.is_java)

    logging.info("Calling coveriteam-service...")
    ret = make_request(args, files)


    if ret.status_code != 200:
        try:
            message = ret.json()["message"]
            logging.error(message)
        except (KeyError, json.JSONDecodeError):
            lines = "\n".join(ret.iter_lines())
            logging.error("There was an error:\n%s", lines)

        archive = Path(f"output-{tool}.zip")
        add_call_data(archive, args, files)
        logging.info(
            "All files used to test the tool "
            "and produce the error  can be found in output-%s.zip",
            tool,
        )
        sys.exit(1)

    result, archive = handle_response(tool, ret)

    add_call_data(archive, args, files)
    logging.info(
        "All files used to test the tool "
        "as well as the results can be found in output-%s.zip",
        tool,
    )
    if result.startswith("true") or result.startswith("false"):
        logging.info("SUCCESS")
        logging.info("Result was: %s", result)
        return

    logging.error("result was not 'true' or 'false': %s", result)
    sys.exit(1)

def main():
    logging.info("Running...")
    branch = os.getenv("CI_MERGE_REQUEST_SOURCE_BRANCH_NAME", "main")

    for tool in get_diff():
        check_tool(tool, branch)


if __name__ == "__main__":
    main()
